import { useEffect, useRef } from 'react'
import PortfolioCard from './PortfolioCard'

const portfolioElements = [
  {
    
    image: '/images/dec.jpg',
    title: 'Decathlon Pro',
    subtitle: 'INSTITUTIONNEL | DEV',
  },
  {
    image:
      '/images/coiff.jpg',
    title: "Revamp Hair",
    subtitle: 'E-COMMERCE | SEA | SEO',
  },
  {
    image:
      'https://cdn.lemon-interactive.fr/wp-content/uploads/2022/09/Image-ressources-humaines-500x309.jpg',
    title: 'Batka',
    subtitle: 'INSTITUTIONNEL | DÉVELOPPEMENT WEB | SEO',
  },
  {
    image:
      '/images/skincare.jpg',
    title: 'Olivier Lemaire',
    subtitle: 'E-COMMERCE | DEV | DÉVELOPPEMENT',
  },
  {
    image:
    '/images/tech.jpg',
    title: 'Tomorrow Tech',
    subtitle: 'E-COMMERCE |DESIGN |DEV',
  },
  {
    image:
      '/images/consult.jpg',
    title: 'Mind7 Consulting',
    subtitle: 'INSTITUTIONNEL | DEV',
  },
  {
    image:
      '/images/deco.jpg',
    title: 'Axodeco',
    subtitle: 'E-COMMERCE | DEV',
  },
  {
    image:
      '/images/dressing.jpg',
    title: 'Devred',
    subtitle: 'E-COMMERCE | DESIGN',
  },
]

const Portfolio = () => {
  const refLeft = useRef(null)
  const refRight = useRef(null)
  useEffect(() => {
    let leftAnimation
    let rightAnimation

    const animateLeft = () => {
      const isMaxScroll =
        refLeft.current.scrollLeft + refLeft.current.clientWidth >=
        refLeft.current.scrollWidth

      refLeft.current.scrollLeft += 1
      if (isMaxScroll) {
        refLeft.current.scroll(0, 0)
      }
      leftAnimation = requestAnimationFrame(animateLeft)
    }
    const animateRight = () => {
      const isMaxScroll =
        Math.abs(refRight.current.scrollLeft - refRight.current.clientWidth) >=
        refRight.current.scrollWidth - 2

      refRight.current.scrollLeft -= 1
      if (isMaxScroll) {
        refRight.current.scroll(0, 0)
      }
      rightAnimation = requestAnimationFrame(animateRight)
    }

    // start animations when the component is mounted and pause them when component is hovered

    // start animation if its not already started
    if (!refLeft.current.hasAttribute('data-started')) {
      refLeft.current.setAttribute('data-started', true)
      animateLeft()
    }
    if (!refRight.current.hasAttribute('data-started')) {
      refRight.current.setAttribute('data-started', true)
      animateRight()
    }

    const handleLeftMouseEnter = () => {
      cancelAnimationFrame(leftAnimation)
    }
    const handleRightMouseEnter = () => {
      cancelAnimationFrame(rightAnimation)
    }

    const handleLeftMouseLeave = () => {
      animateLeft()
    }
    const handleRightMouseLeave = () => {
      animateRight()
    }

    refLeft.current.addEventListener('mouseenter', handleLeftMouseEnter)
    refLeft.current.addEventListener('mouseleave', handleLeftMouseLeave)
    refRight.current.addEventListener('mouseenter', handleRightMouseEnter)
    refRight.current.addEventListener('mouseleave', handleRightMouseLeave)

    return () => {
      cancelAnimationFrame(animateLeft)
      cancelAnimationFrame(animateRight)
    }
  }, [])

  return (
    <section className="font-poppins">
      <div className="mt-20 flex w-full flex-col items-center font-poppins">
        <h2 className="text-paragraph">POURQUOI NOUS CHOISIR</h2>
        <h1 className="mb-[58px] text-[40px] font-bold text-title">
          Un portefeuille client diversifié
        </h1>
      </div>
      <div
        ref={refLeft}
        className="mb-[30px] flex w-full items-center gap-[30px] overflow-x-hidden"
      >
        {portfolioElements.map((element) => (
          <PortfolioCard
            key={element.title}
            image={element.image}
            title={element.title}
            subtitle={element.subtitle}
          />
        ))}
      </div>
      <div
        ref={refRight}
        className="direction-rtl mb-[30px] flex w-full items-center gap-[30px] overflow-x-hidden "
      >
        {portfolioElements.map((element) => (
          <PortfolioCard
            key={element.title}
            image={element.image}
            title={element.title}
            subtitle={element.subtitle}
          />
        ))}
      </div>
    </section>
  )
}

export default Portfolio
