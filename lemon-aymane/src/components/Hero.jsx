import DiagonalArrow from './Icons/DiagonalArrow'
import Lemona from './Icons/Lemona'

const Hero = () => {
  return (
    <section className="mt-16 grid grid-cols-2 gap-y-14 gap-x-[90px] px-[157px] font-poppins text-xl text-paragraph">
      <div>
        <div className="relative z-10">
          <h1 className="mb-5 text-6xl font-extrabold text-[#1C2536]">
            Nos Clients
          </h1>
          <div className="absolute -top-[8px] left-[300px] -z-[1]">
            <Lemona width={58} height={60} fillColor="#EBFA9F" />
          </div>
        </div>

        <p>
          Le point commun de nos clients ? Considérer le site Internet comme un
          véritable outil e-business en plus d’être un moyen de communication
          incontournable
        </p>
      </div>
      <p className="py-[30px]">
        ME, TPE ou grand compte, quel que soit votre projet, nous vous
        accompagnerons dans la démarche de mieux comprendre vos clients en
        ligne, pour mieux les attirer puis leur faire passer les messages
        souhaités ou acheter vos produits !
      </p>
      <div className="relative col-span-2 flex h-[420px] w-full items-end justify-end p-5">
        <img
          className="absolute top-0 left-0 h-full  w-full object-cover"
          src="/images/hero.png"
          alt="hero image"
        />
        <div className="relative flex cursor-pointer items-center justify-center transition-all hover:scale-110">
          <div className="z-30 flex h-[168px] w-[168px] animate-spin-slow items-center justify-center rounded-full bg-[#1C2536] bg-[url('/images/contactus.png')] bg-cover bg-bottom duration-1000 "></div>
          <span className="absolute z-40">
            <DiagonalArrow />
          </span>
        </div>
      </div>
    </section>
  )
}

export default Hero
